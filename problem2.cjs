function problem2(inventory){
    if(!inventory){
        return [];
    }
    if(!inventory.length){
        return [];
    }
    if(inventory.constructor !== Array){
        return [];
    }
    let last = inventory.length - 1;
    return inventory[last];
}
module.exports = problem2;