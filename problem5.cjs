function problem5(inventory){
    if (!inventory || !inventory.length){
        return [];
    }
    if (inventory.constructor !== Array){
        return [];
    }
    let older_cars = [];
    for (let rowIndex=0;rowIndex<inventory.length;rowIndex++){
        if (inventory[rowIndex].car_year<2000){
            older_cars.push(inventory[rowIndex].id)
        }
    }
   
    return older_cars;
}
module.exports = problem5;