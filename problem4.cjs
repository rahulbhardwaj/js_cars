function problem4(inventory){
    if (!inventory || !inventory.length){
        return [];
    }
    if (inventory.constructor !== Array){
        return [];
    }
    let car_years=[];
    for (let rowIndex=0;rowIndex<inventory.length;rowIndex++){
        car_years.push(inventory[rowIndex].car_year)
    }
    car_years.sort();
    return car_years;
}
module.exports = problem4;