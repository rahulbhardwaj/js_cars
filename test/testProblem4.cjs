// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.
let problem4 = require('../problem4.cjs');
let inventory = require('../cars.js');

console.log(problem4(inventory));
console.log(problem4());
console.log(problem4([]));
console.log(problem4(new String("hello")));