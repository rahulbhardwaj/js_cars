let problem2 = require('../problem2.cjs');
let inventory = require('../cars.js');
// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:

console.log(problem2(inventory));
console.log(problem2());
console.log(problem2([]));
console.log(problem2(new String("hello")));