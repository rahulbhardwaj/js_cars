// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
let problem3 = require('../problem3.cjs');
let inventory = require('../cars.js');

console.log(problem3(inventory));
console.log(problem3());
console.log(problem3([]));
console.log(problem3(new String("hello")));