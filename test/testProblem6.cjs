// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

let problem6 = require('../problem6.cjs');
let inventory = require('../cars.js');

console.log(JSON.stringify(problem6(inventory)));
console.log(problem6());
console.log(problem6([]));
console.log(problem6(new String("hello")));