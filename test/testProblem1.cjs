let problem1 = require('../problem1.cjs');
let inventory = require('../cars');
// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:

console.log(problem1(inventory,33));
console.log(problem1());
console.log(problem1(inventory));
console.log(problem1(new String("hello"),33));
console.log(problem1(inventory,[]));
console.log(problem1([],33));
console.log(problem1({id: 33, name: "Test", length: 10}, 33));