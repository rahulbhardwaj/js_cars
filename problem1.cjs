function problem1(inventory,id) {
    if (!inventory){
        return [];
    }
    if (!id && id!==0){
        return [];
    }
    if (inventory.constructor !== Array ){
        return [];
    }
    if (!inventory.length){
        return [];
    }
    if (typeof id !== 'number'){
        return [];
    }
    for(let rowIndex=0;rowIndex<inventory.length;rowIndex++) {
        if (inventory[rowIndex].id===id) {
            return inventory[rowIndex];
        }   
    }
    return ['No Match Found'];
}
module.exports = problem1;