function problem3(inventory){
    if (!inventory || !inventory.length){
        return [];
    }
    if (inventory.constructor !== Array){
        return [];
    }
    let car_models=[];
    for (let rowIndex=0;rowIndex<inventory.length;rowIndex++){
        car_models.push(inventory[rowIndex].car_model)
    }
    car_models.sort();
    return car_models;
}
module.exports = problem3;