function problem6(inventory){
    if (!inventory || !inventory.length){
        return [];
    }
    if (inventory.constructor !== Array){
        return [];
    }
    let bmw_audi_cars = [];
    for (let rowIndex=0;rowIndex<inventory.length;rowIndex++){
        if (inventory[rowIndex].car_make=='Audi' || inventory[rowIndex].car_make=='BMW'){
            bmw_audi_cars.push(inventory[rowIndex]);
        }
    }
   
    return bmw_audi_cars;
}
module.exports = problem6;